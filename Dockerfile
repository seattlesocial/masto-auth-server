FROM docker.io/library/golang:latest AS build
RUN mkdir -p /go/src/gitlab.com/SeattleSocial/masto-auth-server
WORKDIR /go/src/gitlab.com/SeattleSocial/masto-auth-server
COPY * /go/src/gitlab.com/SeattleSocial/masto-auth-server/
RUN CGO_ENABLED=0 go build .

FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /go/src/gitlab.com/SeattleSocial/masto-auth-server/masto-auth-server /masto-auth-server
ENTRYPOINT ["/masto-auth-server"]
