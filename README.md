# masto-auth-service

*an authentication service to authenticate users against a Mastodon server*


Intended for use with [Traefik's Forward Authentication](https://docs.traefik.io/configuration/entrypoints/#forward-authentication)
and similar.
