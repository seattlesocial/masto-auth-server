module gitlab.com/SeattleSocial/masto-auth-server

go 1.19

require (
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.1.3
)

require github.com/gorilla/context v1.1.1 // indirect
