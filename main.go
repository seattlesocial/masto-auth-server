package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/gorilla/sessions"
)

// All headers from Traefik:
//
// X-Forwarded-Uri: /graph?g0.range_input=1h&g0.expr=sum(traefik_backend_open_connections)%20by%20(protocol)&g0.tab=0
// X-Forwarded-Method: GET
// X-Forwarded-Proto: https
// X-Forwarded-For: 10.244.2.1
// X-Forwarded-Host: prometheus.dev1.infra-transition.social.seattle.wa.us

type accessTokenResponse struct {
	AccessToken string `json:"access_token"`
}

// APICredentials stores the credentials required to interact with the mastodon API
type APICredentials struct {
	ClientID     string
	ClientSecret string
}

// Config defines the configuration format
type Config struct {
	Instance             string
	SessionAuthKey       []byte                    // base64 encoded random string used to encrypt cookie data. try go run generate-key.go
	SessionEncryptionKey []byte                    // generated the same way as SessionAuthKey
	EnableWhitelist      bool                      // True to only allow the users listed
	Users                map[string]interface{}    // list of users allowed to access services. A more robust permission structure may eventually be created
	Domains              map[string]APICredentials // Each domain that we're protecting must have API credentials.
}

// User stores everything we need to know about a user
type User struct {
	ID          string `json:"id"`
	Username    string `json:"username"`
	Account     string `json:"acct"`
	DisplayName string `json:"display_name"`
	Avatar      string `json:"avatar"`
	Expires     time.Time
}

var (
	sessionName       = "_masto_auth_server"
	config            Config
	tokenCache        = make(map[string]User)
	store             *sessions.CookieStore
	configPath        = os.Getenv("AUTH_SERVER_CONFIG")
	defaultConfigPath = "auth-server.json"
	postauthPath      = "/-/postauth"
)

func loadConfig() error {
	if configPath == "" {
		configPath = defaultConfigPath
	}
	configFile, err := ioutil.ReadFile(configPath)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(configFile, &config); err != nil {
		return err
	}

	store = sessions.NewCookieStore(config.SessionAuthKey, config.SessionEncryptionKey)
	log.Println("Loaded config.")

	return nil
}

func beginOauthFlow(w http.ResponseWriter, r *http.Request, session *sessions.Session) {
	host := getHost(r)
	redirectURI := fmt.Sprintf("https://%s%s", host, postauthPath)
	clientID := config.Domains[host].ClientID
	query := fmt.Sprintf("response_type=code&client_id=%s&redirect_uri=%s&scopes=read:accounts", url.QueryEscape(clientID), url.QueryEscape(redirectURI))
	session.Values["continue_path"] = r.Header.Get("X-Forwarded-Uri")
	err := session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Fatalf("Failed to save session before redirecting to begin oauth flow: %v", err)
		return
	}
	http.Redirect(w, r, fmt.Sprintf("https://%s/oauth/authorize?%s", config.Instance, query), 307)
}

func validateToken(token string) (User, error) {
	user, ok := tokenCache[token]
	if ok && time.Until(user.Expires) > 0 {
		// cache entry exists, return it
		return user, nil
	}

	log.Println("Validating token with service")

	client := &http.Client{}

	req, err := http.NewRequest("GET", fmt.Sprintf("https://%s/api/v1/accounts/verify_credentials", config.Instance), nil)
	if err != nil {
		return User{}, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	res, err := client.Do(req)
	if err != nil {
		return User{}, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return User{}, err
	}

	err = json.Unmarshal(body, &user)
	if err != nil {
		return User{}, err
	}

	if config.EnableWhitelist {
		if _, ok := config.Users[user.Username]; !ok {
			return User{}, fmt.Errorf("User %s not allowed", user.Username)
		}
	}

	user.Expires = time.Now().Add(time.Hour)

	tokenCache[token] = user

	time.AfterFunc(time.Until(user.Expires), func() {
		delete(tokenCache, token)
	})

	return user, nil
}

func getHost(r *http.Request) string {
	return r.Header.Get("X-Forwarded-Host")
}

func handler(w http.ResponseWriter, r *http.Request) {
	host := getHost(r)
	_, ok := config.Domains[host]
	if !ok {
		log.Printf("invalid domain %s", host)
		http.Error(w, fmt.Sprintf("invalid domain: %s\n", host), http.StatusBadRequest)
		return
	}

	session, err := store.Get(r, sessionName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("Error initializing session: %s", err.Error())
		return
	}

	if strings.HasPrefix(r.Header.Get("X-Forwarded-Uri"), postauthPath) {
		postauthHandler(w, r, session)
		return
	}

	token, ok := session.Values["token"].(string)
	if !ok {
		beginOauthFlow(w, r, session)
		return
	}

	user, err := validateToken(token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("Error validating token: %s", err.Error())
		return
	}

	w.Header().Set("Mastodon-Username", user.Username)
	w.Header().Set("Mastodon-Display-Name", user.DisplayName)
	w.Header().Set("Mastodon-Account", user.Account)
	w.Header().Set("Mastodon-Avatar", user.Avatar)

	fmt.Fprintln(w, "ok")
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			log.Println(r.Method, r.Header.Get("X-Forwarded-Host"), r.Header.Get("X-Forwarded-Uri"), r.Header.Get("X-Forwarded-For"), r.UserAgent())
		}()
		next.ServeHTTP(w, r)
	})
}

func postauthHandler(w http.ResponseWriter, r *http.Request, session *sessions.Session) {
	log.Println("getting access token for user returning from auth")
	host := getHost(r)
	redirectURI := fmt.Sprintf("https://%s%s", host, postauthPath)
	apiCredentials, ok := config.Domains[host]
	if !ok {
		fmt.Fprintln(w, "invalid domain")
		return
	}

	uri, err := url.ParseRequestURI(r.Header.Get("X-Forwarded-Uri"))
	if err != nil {
		log.Printf("Error parsing requested URI: %s", err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := http.PostForm(fmt.Sprintf("https://%s/oauth/token", config.Instance), url.Values{
		"client_id":     {apiCredentials.ClientID},
		"client_secret": {apiCredentials.ClientSecret},
		"grant_type":    {"authorization_code"},
		"code":          {uri.Query().Get("code")},
		"redirect_uri":  {redirectURI},
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	token := accessTokenResponse{}
	if err = json.Unmarshal(body, &token); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["token"] = token.AccessToken
	continuePath := session.Values["continue_path"].(string)
	delete(session.Values, "continue_path")
	if err := session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("Failed to save new session: %s", err.Error())
		return
	}

	if !strings.HasPrefix(continuePath, "/") {
		log.Printf("Invalid continuePath: %s", continuePath)
		continuePath = "/"
	}

	continuePath = fmt.Sprintf("%s://%s%s", r.Header.Get("X-Forwarded-Proto"), r.Header.Get("X-Forwarded-Host"), continuePath)
	log.Printf("Successfully authenticated user. Redirecting to %s", continuePath)
	http.Redirect(w, r, continuePath, 307)
}

func main() {
	if err := loadConfig(); err != nil {
		panic(err)
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/", handler)
	http.ListenAndServe(":8080", loggingMiddleware(mux))
}
